# GDocs Converter

Wie können GoogleDocs Dokumente in Markdown konvertiert werden? Ganz einfach:

Mit Hilfe des Google Apps script [gdocs2md](https://github.com/mangini/gdocs2md) können Google Drive Dokumente zu Markdown konvertiert werden. So geht es:

1. Das Google Doc aufrufen.
1. Das [Google Apps Skript](https://raw.githubusercontent.com/mangini/gdocs2md/master/converttomarkdown.gapps) kopieren.
1. Über den Reiter "Tools" den Skripteditor aufrufen. ![Screenshot](images/gdocs-1.png)
1. Die Template-Funktion löschen und das Google Apps Skript einfügen.
1. Das Skript speichern (bspw. "Markdown-Export-Skript"). ![Screenshot](images/gdocs-2.png)
1. Wieder das Google Doc öffnen und über "Tool" den Skripteditor öffnen.
1. Über "Select function" "Convert to Markdown" wählen. ![Screenshot](images/gdocs-3.png)
1. Die Markdown Dateien und darin enthaltenen Bilder werden an die eigene Google-Mail-Adresse gesendet. ![Screenshot](images/gdocs-4.png)

## Hier gibt es das Dokument in [HTML](https://sroertgen.gitlab.io/gdocs-converter/index.html), [PDF](https://sroertgen.gitlab.io/gdocs-converter/course.pdf) oder als [EPUB](https://sroertgen.gitlab.io/gdocs-converter/course.epub)
